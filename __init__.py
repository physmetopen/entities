# -*- coding: utf-8 -*-
from __future__ import absolute_import

import os

import softpy

thisdir = os.path.abspath(os.path.dirname(__file__))

softpy.register_jsondir(thisdir)
