Public entities for physical metallurgy
=======================================
A set of entities for defining a microstructure (material state) and
properties that can be calculated from it.  These entities provides
the basis for an microstructure ontology for CALM.


Chemistry*
----------
A set of generic entities describing the chemical composition of an
alloy and its intermetallic phases at different levels of details,
including:

  - **Chemistry**: Averaged alloy and particle compositions. No particle
    size or spatial information. (PrecipAl)

  - **ChemistryLognormal**: Lognormal size distribution of each particle
    type. No spatial information.

  - **ChemistrySizeDistribution**: Discritised size distribution of each
    particle type. Introduses domains. No spatial information.
    (NaMo, Premium, PreciMS)

  - **ChemistrySizeDistributionSpatial**: Set of domains with lognormal
    particle size distributions.  Each domain is associated to both a
    spatial resolved cell and a phase.  Hence, within a cell, the
    size distribution of phase may be described by the superposition of
    several lognormal distributions. (gbseg)

  - **ChemistrySpatial**: Position, size and type of each particle.
    (processed SPED data)

  - **ChemistryVoxel**: Atom and phase concentration in each voxel.
    (raw SPED data).

A set of translators will be provided to translate between the different
chemistry-related entities.


Microstructure
--------------
An entity that together with the chemistry describes general aspects of
a microstructure state.  Detailed information about texture, particle
size distributions, etc. should probably have their own dedicated
entities.


MaterialParameters
------------------
Generic material parameters.

QUESTION: The Taylor factor should probably be considered as a property that
can be calculated from the microstructure rather than a material parameter?


Constants
---------
Common physical (and mathematical) constants.  Not needed for
interoperability, but provided for convenience. Only one instance of
this entity is needed.


ExternalParameters
------------------
Generic entity describing an instant value of external variables for
the system.
